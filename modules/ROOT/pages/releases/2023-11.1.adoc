= Testing Farm release 2023-11.1
:keywords: Releases

Testing Farm release https://issues.redhat.com/projects/TFT/versions/12413646[2023-11.1] is deployed 🎉.


== Upgrades

* https://gitlab.com/testing-farm/artemis[Artemis] upgraded from v0.0.64 to https://gitlab.com/testing-farm/artemis/-/releases/v0.0.66[v0.0.66], see also release notes for https://gitlab.com/testing-farm/artemis/-/releases/v0.0.65[v0.0.65].

* Testing Farm CLI tool updated from v0.0.14 to https://gitlab.com/testing-farm/cli/-/releases/v0.0.15[v0.0.15], please update your installation for the new features to work.


== Highlights

* Test checks from `tmt` are now properly displayed in Testing Farm results. (https://issues.redhat.com/browse/TFT-1666[TFT-1666])

* Provisioning retries can be disabled and provisioning errors can be treated as test failures. (https://issues.redhat.com/browse/TFT-2202[TFT-2202])

* It is now possible to set a backlink to user's CI system or service via the API. (https://issues.redhat.com/browse/TFT-1452[TFT-1452])

* Testing Farm now supports Red Hat CoreOS images. (https://issues.redhat.com/browse/TFT-1697[TFT-1697])

* Debug and event logs from Flasher, used to provision RHIVOS devices, are now exposed in the test runs. (https://issues.redhat.com/browse/TFT-2247[TFT-2247])

* The CLI tool now supports printing only request ID in its `reserve` command. (https://issues.redhat.com/browse/TFT-2249[TFT-2249])

* Polarion and ReportPortal integration is now https://docs.testing-farm.io/Testing%20Farm/0.1/test-request.html#_reporting[documented] in Testing Farm docs. (https://issues.redhat.com/browse/TFT-2263[TFT-2263])

* The `skip` test result outcome is now supported. (https://issues.redhat.com/browse/TFT-2282[TFT-2282])


== Bugfixes

* RHEL repositories in Beaker provisioned machines now use `rhel-` prefix. (https://issues.redhat.com/browse/TFT-2098[TFT-2098])

* The `tmt-reboot` command now works during https://docs.testing-farm.io/Testing%20Farm/0.1/cli.html#reserve[Testing Farm reservations]. (https://issues.redhat.com/browse/TFT-2217[TFT-2217])

* Kernel parameters can be now passed via Testing Farm API. (https://issues.redhat.com/browse/TFT-2376[TFT-2376])

* Testing Farm sets `tmt` context during plan export. (https://issues.redhat.com/browse/TFT-2383[TFT-2383])

* Console logs are now enabled in Public ranch. (https://issues.redhat.com/browse/TFT-2378[TFT-2378])


== Contributors

We would like to thank the following external contributors for contributing to Testing Farm.

* Ed Santiago - for contributing to our CLI tool


== Packages

List of important packages bundled in the worker image.

[source,shell]
....
❯ podman run --entrypoint rpm quay.io/testing-farm/worker:2023-11.1 -q tmt standard-test-roles ansible-core podman beakerlib | sort | uniq
ansible-core-2.14.11-1.fc38.noarch
beakerlib-1.29.3-2.fc38.noarch
podman-4.7.2-1.fc38.x86_64
standard-test-roles-4.11-2.fc38.noarch
tmt-1.29.0-1.fc38.noarch
....


== Statistics

* Testing Farm has passed https://stats.testing-farm.io/d/dpYooDIVk/testing-farm-all-time-stats?orgId=1&viewPanel=7[725k requests] per year \o/

* http://metrics.osci.redhat.com/d/NnHWU1dnz/testing-farm?viewPanel=12&orgId=1&from=1698793200000&to=1701385200000[Error rate ~ 2.67% from start of November]

* http://metrics.osci.redhat.com/d/NnHWU1dnz/testing-farm?viewPanel=8&orgId=1&from=1698793200000&to=1701385200000[~ 64k testing requests in November]

* Average queue time http://metrics.osci.redhat.com/d/NnHWU1dnz/testing-farm?viewPanel=26&orgId=1&from=1698793200000&to=1701385200000[20.3s] on Public ranch, http://metrics.osci.redhat.com/d/NnHWU1dnz/testing-farm?viewPanel=27&orgId=1&from=1698793200000&to=1701385200000[12.1s] on Red Hat ranch from the start of October.
