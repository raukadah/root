.DEFAULT_GOAL := help
.PHONY: clean help generate

GENERATED_ROOT ?= public
RFD_DIR = modules/ROOT/pages/rfd

# Help prelude
define PRELUDE

Usage:
  make [target]

endef

##@ Utility

generate:  ## Generate documentation
	rm -rf $(RFD_DIR) && mkdir -p $(RFD_DIR)
	curl -s "https://gitlab.com/api/v4/projects/14474455/repository/archive.tar.gz?path=rfd" | tar -C $(RFD_DIR) -xzv --strip-components 2 -f -
	mv $(RFD_DIR)/*.png modules/ROOT/images
	python3 scripts/generate-rfd-navigation.py
	npx antora --fetch --cache-dir .cache/antora --attribute page-pagination= --redirect-facility gitlab --to-dir $(GENERATED_ROOT) antora-playbook.yml

clean:  ## Cleanup
	rm -rf .cache
	rm -rf public

# See https://www.thapaliya.com/en/writings/well-documented-makefiles/ for details.
reverse = $(if $(1),$(call reverse,$(wordlist 2,$(words $(1)),$(1)))) $(firstword $(1))

help:  ## Show this help
	@awk 'BEGIN {FS = ":.*##"; printf "$(info $(PRELUDE))"} /^[a-zA-Z_/-]+:.*?##/ { printf "  \033[36m%-35s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(call reverse, $(MAKEFILE_LIST))
